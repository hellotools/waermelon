/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'WaterMelon',
  tagline: 'A project :-)',
  url: 'https://hellotools.gitlab.io',
  baseUrl: '/watermelon/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'HelloTools', // Usually your GitHub org/user name.
  projectName: 'Watermelon', // Usually your repo name.
  themes: ['@docusaurus/theme-live-codeblock'],
  i18n: {
    defaultLocale: 'zh-CN',
    // eslint-disable-next-line no-nested-ternary
    //locales: ['en'],
  },
  plugins: [
    [
      'ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        // disableInDev: false,
      },
    ],
  ],
  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        debug: true, // force debug plugin usage
        docs: {
          // routeBasePath: '/',
          path: 'docs',
          sidebarPath: 'sidebars.js',
          // sidebarCollapsible: false,
          // sidebarCollapsed: true,
          editUrl: ({locale, docPath}) => {
            return `https://gitlab.com/facebook/docusaurus/-/edit/main/website/${nextVersionDocsDirPath}/${docPath}`;
          },
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        blog: {
          // routeBasePath: '/',
          path: 'blog',
          editUrl: ({locale, blogDirPath, blogPath}) => {
            return `https://gitlab.com/facebook/docusaurus/-/edit/main/website/${blogDirPath}/${blogPath}`;
          },
          postsPerPage: 8,
          feedOptions: {
            type: 'all',
            copyright: `Copyright © ${new Date().getFullYear()} HelloTools.`,
          },
          blogSidebarCount: 'ALL',
          blogSidebarTitle: 'All our posts',
        },
        pages: {

        },
        theme: {
          customCss: [require.resolve('./src/css/custom.css')],
        },
      }),
    ],
  ],
  themeConfig: {
    announcementBar: {
        id: 'announcementBar-1', // Increment on change
        content: `⭐️ If you like WaterMelon, give it a star on <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/hellotools/watermelon">GitLab</a> and follow us.`,
    },
    navbar: {
      title: 'Watermelon Doc',
      logo: {
        alt: 'Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: '文档',
          position: 'left',
        },
        {
          to: 'blog',
          label: '博客',
          position: 'left'
        },
        {
          href: 'https://gitlab.com/helotools/watermelon',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: '文档',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: '社群',
          items: [
            {
              label: 'Gitter',
              href: 'https://gitter.im/watermelon/',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/hellotools',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/hellotools/watermelon',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} .Built with Love.`,
    },
  },
};


async function createConfig() {
  //Do some things
  return config;
}

module.exports = createConfig;


